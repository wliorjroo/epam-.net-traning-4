﻿using DAL.DataSource.Factory;
using Ninject.Extensions.Factory;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DataSource.DI.Ninject
{
    public class DataSourceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<DbContext>().To<MarketCompany>().InSingletonScope();
            Bind<IDbContextFactory>().ToFactory();
        }
    }
}
