﻿using BL.FileReader;
using BL.FileWatcher;
using BL.FolderReader;
using BL.Parser;
using DAL.DI.Ninject;
using DAL.Model;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DI.Ninject
{
    public class BLModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Load(new[] { new DALModule() });

            Bind<IFileReader>().To<FileReader.CSVReader>().InSingletonScope();
            Bind<IParser<Order>>().To<OrderParser>().InSingletonScope();
            Bind<IFileWatcher>().To<CSVWatcher>();
            Bind<IFolderReader>().To<CSVInFolderReader>();
        }
    }
}
