﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.FileReader
{
    public interface IFileReader
    {
        void ReadFile(string filePath);
    }
}
