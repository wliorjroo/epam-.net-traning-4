﻿using BL.Parser;
using DAL.Model;
using DAL.RepositoryContainer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.FileReader
{
    class CSVReader : IFileReader
    {
        private readonly IParser<Order> _parser;
        private readonly ISameContextRepositoryContainer _repositoryContainer;

        public CSVReader(ISameContextRepositoryContainer repositoryContainer, IParser<Order> parser)
        {
            _repositoryContainer = repositoryContainer ?? throw new ArgumentNullException(nameof(repositoryContainer));
            _parser = parser ?? throw new ArgumentNullException(nameof(parser));//new OrderParser(_repositoryContainer);
        }

        public void ReadFile(string filePath)
        {
            using(StreamReader streamReader = new StreamReader(File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.None)))
            {
                var orderRepository = _repositoryContainer.GetRepository<Order>();
                while (!streamReader.EndOfStream)
                {
                    var order = _parser.Parse(streamReader.ReadLine());
                    orderRepository.Create(order);
                }
                _repositoryContainer.ContextSave();
            }
        }
    }
}
