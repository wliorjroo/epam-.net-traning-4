﻿using BL.Parser.Exception;
using DAL.Model;
using DAL.RepositoryContainer;
using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Parser
{
    class OrderParser : Parser<Order>
    {
        private readonly char _splitSeparator = ';';
        private Action<string, Order>[] ParseActions;

        public OrderParser(ISameContextRepositoryContainer container) : base(container)
        {
            InitialzeeActions();
        }

        private void InitialzeeActions()
        {
            ParseActions = new Action<string, Order>[]
            {
                (date, order) => order.Date = ParseDate(date),
                (customerId, order) => 
                {
                    order.Customer_Id = ParseInt(customerId);
                    order.Customer = Parse<Customer>(customerId);
                },
                (productId, order) => 
                {
                    order.Product_Id = ParseInt(productId);
                    order.Product = Parse<Product>(productId);
                },
                (count, order) => order.ProductCount = ParseInt(count),
                (sellerId, order) => 
                {
                    order.Seller_Id = ParseInt(sellerId);
                    order.Seller = Parse<Seller>(sellerId);
                },
            };
        }

        public override Order Parse(string model)
        {
            var result = new Order();
            var items = model.Split(_splitSeparator);
            if (items.Length != ParseActions.Length)
                throw new ParseException();

            for (int i = 0; i < items.Length; i++)
            {
                ParseActions[i].Invoke(items[i], result);
            }
            return result;
        }
    }
}
