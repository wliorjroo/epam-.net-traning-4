﻿using BL.Parser.Exception;
using DAL.RepositoryContainer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Parser
{
    abstract class Parser<Model> : IParser<Model> where Model : class
    {
        private IRepositoryContainer _container;

        protected Parser(ISameContextRepositoryContainer container)
        {
            _container = container ?? throw new ArgumentNullException(nameof(container));
        }

        public abstract Model Parse(string model);

        protected AssociatedModel Parse<AssociatedModel>(string modelId, bool isModelNotNull = true) where AssociatedModel : class
        {
            int id = ParseInt(modelId);
            var model = _container.GetRepository<AssociatedModel>().Get(id);
            if (isModelNotNull && model == null)
            {
                throw new ParseException();
            }
            return model;
        }

        protected int ParseInt(string number)
        {
            int result;
            if (int.TryParse(number, out result))
            {
                return result;
            }
            else
            {
                throw new ParseException();
            }
        }

        protected DateTime ParseDate(string date)
        {
            DateTime result;
            if (DateTime.TryParse(date, out result))
            {
                return result;
            }
            else
            {
                throw new ParseException();
            }
        }
    }
}
