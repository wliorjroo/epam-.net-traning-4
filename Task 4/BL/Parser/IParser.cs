﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Parser
{
    interface IParser<Model>
    {
        Model Parse(string model);
    }
}
