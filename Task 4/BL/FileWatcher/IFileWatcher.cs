﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.FileWatcher
{
    public interface IFileWatcher : IDisposable
    {
        bool IsWatching { get; }
        void BeginWatch(DirectoryInfo readDirectory, DirectoryInfo parsedFilesDirectory, DirectoryInfo notParsedFilesDirectory);
        void EndWatch();
    }
}
