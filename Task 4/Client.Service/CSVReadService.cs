﻿using BL.FileWatcher;
using BL.FolderReader;
using Client.Base.DI.Ninject;
using Client.Base.Parameters;
using Client.Base.Parameters.Parser.Exceptions;
using Ninject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Client.Service
{
    public partial class CSVReadService : ServiceBase
    {
        private Task _watchTask;
        private CancellationTokenSource _tokenSource;
        private IFileWatcher _fileWatcher;
        private IKernel _kernel;

        public CSVReadService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _tokenSource = new CancellationTokenSource();
            _watchTask = Task.Factory.StartNew(StartService, args, _tokenSource.Token);
        }

        protected override void OnStop()
        {
            if (_watchTask != null)
            {
                if (_watchTask.IsCompleted)
                {
                    _fileWatcher.EndWatch();
                    _fileWatcher.Dispose();
                    _tokenSource.Dispose();
                    _kernel.Dispose();
                }
                else
                {
                    _tokenSource.Cancel();
                    try
                    {
                        _watchTask.Wait();
                    }
                    catch (AggregateException)
                    {
                        // TODO excaption handling
                    }
                    finally
                    {
                        _tokenSource.Dispose();
                    }

                    if (_kernel != null)
                        _kernel.Dispose();
                    if (_fileWatcher != null)
                        _fileWatcher.Dispose();
                }

                _fileWatcher = null;
                _kernel = null;
                _tokenSource = null;
                _watchTask = null;
            }
        }

        private void StartService(object arg)
        {
            _tokenSource.Token.ThrowIfCancellationRequested();

            string[] args = (string[])arg;
            DirectoryInfo folderPath, parsedPath, notParsedPath;
            _kernel = new StandardKernel(new ClientBaseModule());

            IParamParser parser = _kernel.Get<IParamParser>();
            parser.Parse(args);
            folderPath = parser.FolderPath;
            parsedPath = parser.ParsedPath;
            notParsedPath = parser.NotParsedPath;

            _tokenSource.Token.ThrowIfCancellationRequested();

            IFolderReader folderReader = _kernel.Get<IFolderReader>();
            folderReader.ReadFiles(folderPath, parsedPath, notParsedPath);
            folderReader.Dispose();

            _tokenSource.Token.ThrowIfCancellationRequested();

            _fileWatcher = _kernel.Get<IFileWatcher>();
            _fileWatcher.BeginWatch(folderPath, parsedPath, notParsedPath);
        }
    }
}
