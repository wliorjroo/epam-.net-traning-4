﻿using Client.Base.Configuration;
using Client.Base.DI.Ninject;
using Ninject;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace Client.Service
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();

            IKernel kernel = new StandardKernel(new ClientBaseModule());
            var settings = kernel.Get<ISettings>();
            serviceInstaller1.ServiceName = settings.GetServiceName();
        }
    }
}
