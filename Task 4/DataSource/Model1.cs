namespace DataSource
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    //public partial class Model1 : DbContext
    //{
    //    public Model1()
    //        : base("name=Model1")
    //    {
    //    }

    //    public virtual DbSet<ClientSet> ClientSet { get; set; }
    //    public virtual DbSet<OrderSet> OrderSet { get; set; }
    //    public virtual DbSet<ProductSet> ProductSet { get; set; }
    //    public virtual DbSet<UserSet> UserSet { get; set; }

    //    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    //    {
    //        modelBuilder.Entity<ClientSet>()
    //            .HasMany(e => e.OrderSet)
    //            .WithRequired(e => e.ClientSet)
    //            .HasForeignKey(e => e.Client_Id)
    //            .WillCascadeOnDelete(false);

    //        modelBuilder.Entity<ProductSet>()
    //            .HasMany(e => e.OrderSet)
    //            .WithRequired(e => e.ProductSet)
    //            .HasForeignKey(e => e.Product_Id)
    //            .WillCascadeOnDelete(false);

    //        modelBuilder.Entity<UserSet>()
    //            .HasMany(e => e.OrderSet)
    //            .WithRequired(e => e.UserSet)
    //            .HasForeignKey(e => e.User_Id)
    //            .WillCascadeOnDelete(false);
    //    }
    //}
}
