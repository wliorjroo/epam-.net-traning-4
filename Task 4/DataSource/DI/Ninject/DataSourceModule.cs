﻿using DataSource.Factory;
using Ninject.Extensions.Factory;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataSource.DI.Ninject
{
    public class DataSourceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<DbContext>().To<TestModel>().InSingletonScope();
            Bind<IContextFactory>().ToFactory();
        }
    }
}
