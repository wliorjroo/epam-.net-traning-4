﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataSource.Factory
{
    public interface IContextFactory
    {
        DbContext Create();
    }
}
