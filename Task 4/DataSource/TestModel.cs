﻿namespace DataSource
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class TestModel : DbContext
    {
        // Контекст настроен для использования строки подключения "TestModel" из файла конфигурации  
        // приложения (App.config или Web.config). По умолчанию эта строка подключения указывает на базу данных 
        // "DataSource.TestModel" в экземпляре LocalDb. 
        // 
        // Если требуется выбрать другую базу данных или поставщик базы данных, измените строку подключения "TestModel" 
        // в файле конфигурации приложения.
        public TestModel()
            : base("name=TestModel")
        {
            Database.Log = s => Console.WriteLine(s);
        }

        // Добавьте DbSet для каждого типа сущности, который требуется включить в модель. Дополнительные сведения 
        // о настройке и использовании модели Code First см. в статье http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<ClientSet> ClientSet { get; set; }
        public virtual DbSet<ProductSet> ProductSet { get; set; }
        public virtual DbSet<UserSet> UserSet { get; set; }
        public virtual DbSet<OrderSet> OrderSet { get; set; }
    }
}