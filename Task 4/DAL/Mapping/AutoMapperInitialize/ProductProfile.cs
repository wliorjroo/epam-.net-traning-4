﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Mapping.Initialize
{
    class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<Model.Product, DataSource.Product>();
            CreateMap<DataSource.Product, Model.Product>();
        }
    }
}
