﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Mapping.Initialize
{
    static class AutoMapperFactory
    {
        private static IMapper _mapper;

        public static IMapper Create()
        {
            if (_mapper == null)
            {
                var configure = new MapperConfiguration(Create);
                configure.CompileMappings();
                _mapper = configure.CreateMapper();
            }
            return _mapper;
        }

        private static void Create(IMapperConfigurationExpression configure)
        {
            configure.AddProfile<ClientProfile>();
            configure.AddProfile<OrderProfile>();
            configure.AddProfile<ProductProfile>();
            configure.AddProfile<UserProfile>();
        }
    }
}
