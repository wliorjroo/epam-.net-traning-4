﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Mapping.Initialize
{
    class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateDataToModel();
            CreateModelToData();
        }

        private void CreateDataToModel()
        {
            var mapping = CreateMap<DataSource.Order, Model.Order>();
            mapping.ForMember(d => d.Customer_Id, m => m.MapFrom(s => s.Client_Id));
            mapping.ForMember(d => d.Product_Id, m => m.MapFrom(s => s.Product_Id));
            mapping.ForMember(d => d.Seller_Id, m => m.MapFrom(s => s.User_Id));
        }

        private void CreateModelToData()
        {
            var mapping = CreateMap<Model.Order, DataSource.Order>();
            mapping.ForMember(d => d.Client_Id, m => m.MapFrom(s => s.Customer_Id));
            mapping.ForMember(d => d.Product_Id, m => m.MapFrom(s => s.Product_Id));
            mapping.ForMember(d => d.User_Id, m => m.MapFrom(s => s.Seller_Id));
        }
    }
}
