﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DAL.Model;
using DAL.DataSource;

namespace DAL.Mapping.Initialize
{
    class ClientProfile : Profile
    {
        public ClientProfile()
        {
            CreateMap<Client, Customer>();
            CreateMap<Customer, Client>();
        }
    }
}
