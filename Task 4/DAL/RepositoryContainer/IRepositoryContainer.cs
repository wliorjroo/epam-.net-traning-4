﻿using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.RepositoryContainer
{
    public interface IRepositoryContainer
    {
        IRepository<Model> GetRepository<Model>() where Model : class;
    }
}
