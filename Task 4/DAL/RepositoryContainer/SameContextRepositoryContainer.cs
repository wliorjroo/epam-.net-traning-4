﻿using System;
using System.Data.Entity;
using System.Threading;
using DAL.Repository;
using DAL.Repository.Factory;

namespace DAL.RepositoryContainer
{
    class SameContextRepositoryContainer : ISameContextRepositoryContainer
    {
        private readonly DbContext _dbContext;
        private readonly IRepositoryFactory _repositoryFactory;
        private readonly ReaderWriterLockSlim _readerWriterLock;
        private bool _disposedValue = false;

        public SameContextRepositoryContainer(DbContext dbContext, IRepositoryFactory repositoryFactory, ReaderWriterLockSlim readerWriterLock) 
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _repositoryFactory = repositoryFactory ?? throw new ArgumentNullException(nameof(repositoryFactory));
            _readerWriterLock = readerWriterLock ?? throw new ArgumentNullException(nameof(readerWriterLock));
        }

        public void ContextSave()
        {
            _readerWriterLock.EnterWriteLock();
            try
            {
                _dbContext.SaveChanges();
            }
            finally
            {
                _readerWriterLock.ExitWriteLock();
            }
        }

        public IRepository<Model> GetRepository<Model>() where Model : class
        {
            return _repositoryFactory.CreateRepository<Model>();
        }

        #region IDisposable Support
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
