﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    class AutoMappedRepository<Model, Entity> : IRepository<Model> where Model : class where Entity : class
    {
        private readonly IRepository<Entity> _repository;
        private readonly IMapper _mapper;

        public AutoMappedRepository(IRepository<Entity> repository, IMapper mapper)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public void Create(Model item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            _repository.Create(_mapper.Map<Model, Entity>(item));
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }

        public Model Get(int id)
        {
            var entity = _repository.Get(id);
            if (entity != null)
                return _mapper.Map<Entity, Model>(entity);
            return null;
        }

        public void Update(Model item)
        {
            if (item != null)
                throw new ArgumentNullException(nameof(item));

            _repository.Update(_mapper.Map<Model, Entity>(item));
        }
    }
}
