﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DAL.Repository
{
    class Repository<Entity> : IRepository<Entity> where Entity : class
    {
        private readonly DbSet<Entity> _dbSet;
        private readonly DbContext _dbContext;
        private readonly ReaderWriterLockSlim _readerWriterLock;

        public Repository(DbContext dbContext, ReaderWriterLockSlim readWriteLock)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _readerWriterLock = readWriteLock ?? throw new ArgumentNullException(nameof(readWriteLock));
            _dbSet = dbContext.Set<Entity>();
        }

        public void Create(Entity item)
        {
            _readerWriterLock.EnterWriteLock();
            try
            {
                _dbSet.Add(item);
            }
            finally
            {
                _readerWriterLock.ExitWriteLock();
            }
        }

        public void Delete(int id)
        {
            _readerWriterLock.EnterUpgradeableReadLock();
            try
            {
                var entity = Get(id);
                if (entity != null)
                {
                    _readerWriterLock.EnterWriteLock();
                    try
                    {
                        _dbSet.Remove(entity);
                    }
                    finally
                    {
                        _readerWriterLock.ExitWriteLock();
                    }
                }
            }
            finally
            {
                _readerWriterLock.ExitUpgradeableReadLock();
            }
        }

        public Entity Get(int id)
        {
            _readerWriterLock.EnterReadLock();
            try
            {
                return _dbSet.Find(id);
            }
            finally
            {
                _readerWriterLock.ExitReadLock();
            }
        }

        public void Update(Entity item)
        {
            _readerWriterLock.EnterWriteLock();
            try
            {
                _dbSet.Attach(item);
                _dbContext.Entry(item).State = EntityState.Modified;
            }
            finally
            {
                _readerWriterLock.ExitWriteLock();
            }
        }
    }
}
