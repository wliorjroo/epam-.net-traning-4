﻿using AutoMapper;
using DAL.Mapping.Initialize;
using DAL.Repository;
using DAL.Repository.Factory;
using DAL.RepositoryContainer;
using DAL.Unity;
using Ninject.Extensions.Factory;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DAL.DI.Ninject
{
    public class DALModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Load(new[] { new DataSource.DI.Ninject.DataSourceModule() });

            Bind<IMapper>().ToConstant(AutoMapperFactory.Create());

            Bind<IRepository<DataSource.Client>>().To<Repository<DataSource.Client>>().InThreadScope();
            Bind<IRepository<DataSource.Order>>().To<Repository<DataSource.Order>>().InThreadScope();
            Bind<IRepository<DataSource.Product>>().To<Repository<DataSource.Product>>().InThreadScope();
            Bind<IRepository<DataSource.User>>().To<Repository<DataSource.User>>().InThreadScope();

            Bind<IRepository<Model.Customer>>().To<AutoMappedRepository<Model.Customer, DataSource.Client>>().InThreadScope();
            Bind<IRepository<Model.Order>>().To<AutoMappedRepository<Model.Order, DataSource.Order>>().InThreadScope();
            Bind<IRepository<Model.Product>>().To<AutoMappedRepository<Model.Product, DataSource.Product>>().InThreadScope();
            Bind<IRepository<Model.Seller>>().To<AutoMappedRepository<Model.Seller, DataSource.User>>().InThreadScope();

            Bind<IRepositoryFactory>().ToFactory();

            Bind<ReaderWriterLockSlim>().To<Locker>().InSingletonScope();
            Bind<ISameContextRepositoryContainer>().To<SameContextRepositoryContainer>().InThreadScope();
        }
    }
}
