﻿using Client.Base.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Client.Console.Service
{
    class Service
    {
        private readonly ISettings _settings;
        private readonly ServiceController _service;

        public Service(ISettings settings)
        {
            _settings = settings ?? throw new ArgumentNullException(nameof(settings));
            _service = Initialize();
        }

        public bool IsRunning
        {
            get {
                if (_service == null)
                    return false;

                _service.Refresh();
                if (_service.Status == ServiceControllerStatus.ContinuePending || _service.Status == ServiceControllerStatus.Running || _service.Status == ServiceControllerStatus.StartPending)
                {
                    return true;
                }
                return false;
            }
        }

        public bool IsInstailed
        {
            get {
                return _service != null;
            }
        }

        public bool CanStop
        {
            get {
                return IsInstailed && _service.CanStop;
            }
        }

        private ServiceController Initialize()
        {
            string serviceName = _settings.GetServiceName();
            ServiceController result = null;
            ServiceController[] services = ServiceController.GetServices();
            foreach (ServiceController service in services)
            {
                if (service.ServiceName == serviceName)
                {
                    result = service;
                }
            }

            return result;
        }

        public void Stop()
        {
            if (IsRunning && _service.CanStop)
            {
                _service.Stop();
                _service.WaitForStatus(ServiceControllerStatus.Stopped);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public void Start(string[] args = null)
        {
            if (!IsRunning && _service != null)
            {
                if (args != null)
                {
                    _service.Start(args);
                }
                else
                {
                    _service.Start();
                }
            }
            else
            {
                throw new InvalidOperationException();
            }
        }
    }
}
