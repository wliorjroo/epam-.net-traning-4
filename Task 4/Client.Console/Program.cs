﻿using Client.Base.DI.Ninject;
using Client.Base.Parameters;
using Client.Base.Parameters.Parser.Exceptions;
using Ninject;
using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.FolderReader;
using BL.FileWatcher;
using Client.Base.Configuration;

namespace Client.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                DirectoryInfo folderPath, parsedPath, notParsedPath;
                IKernel kernel = new StandardKernel(new ClientBaseModule());

                try
                {
                    IParamParser parser = kernel.Get<IParamParser>();
                    parser.Parse(args);
                    folderPath = parser.FolderPath;
                    parsedPath = parser.ParsedPath;
                    notParsedPath = parser.NotParsedPath;
                }
                catch (ParseParamException exception)
                {
                    ConsoleWriteExitMessage(exception.Message);
                    return;
                }

                Service.Service service = new Service.Service(kernel.Get<ISettings>());
                if (service.IsInstailed)
                {
                    if (service.CanStop)
                    {
                        WriteMessage("Stoping service...");
                        service.Stop();
                        WriteMessage("Service stoped.");
                    }
                    else
                    {
                        ConsoleWriteExitMessage("Can't stop service.");
                        return;
                    }
                }

                IFolderReader folderReader = kernel.Get<IFolderReader>();
                folderReader.ReadFiles(folderPath, parsedPath, notParsedPath);
                folderReader.Dispose();

                IFileWatcher fileWatcher = kernel.Get<IFileWatcher>();
                fileWatcher.BeginWatch(folderPath, parsedPath, notParsedPath);
                ConsoleWriteExitMessage("Watching for files in directory: " + folderPath);
                fileWatcher.EndWatch();
                fileWatcher.Dispose();

                if (service.IsInstailed)
                {
                    WriteMessage("Starting service...");
                    service.Start();
                    WriteMessage("Service started.");
                }
            } 
            catch
            {
                ConsoleWriteExitMessage("Unhandled error has occurred!");
            }
        }

        private static void ConsoleWriteExitMessage(string message)
        {
            System.Console.WriteLine(message);
            System.Console.WriteLine("Press any key to exit.");
            System.Console.ReadKey();
        }

        private static void WriteMessage(string message)
        {
            System.Console.WriteLine(message);
        }
    }
}
