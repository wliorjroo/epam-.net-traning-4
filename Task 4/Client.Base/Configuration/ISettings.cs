﻿namespace Client.Base.Configuration
{
    public interface ISettings
    {
        string GetFolderPath();
        string GetNotParsedPath();
        string GetParsedPath();
        string GetServiceName();
    }
}