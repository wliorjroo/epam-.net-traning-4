﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Client.Base.Configuration
{
    class Settings : ISettings
    {
        private readonly System.Configuration.Configuration _config;

        public Settings()
        {
            Assembly assembly = Assembly.GetAssembly(GetType());
            _config = ConfigurationManager.OpenExeConfiguration(assembly.Location);
        }

        public string GetFolderPath()
        {
            return GetConfigurationValue("WatchPath");
        }

        public string GetParsedPath()
        {
            return GetConfigurationValue("ParsedFilesFolder");
        }

        public string GetNotParsedPath()
        {
            return GetConfigurationValue("NotParsedFilesFolder");
        }

        public string GetServiceName()
        {
            return GetConfigurationValue("ServiceName"); ;
        }

        private string GetConfigurationValue(string key)
        {
            if (_config.AppSettings.Settings[key] != null)
            {
                return _config.AppSettings.Settings[key].Value;
            }
            else
            {
                throw new IndexOutOfRangeException("Settings collection does not contain the requested key: " + key);
            }
        }
    }
}
